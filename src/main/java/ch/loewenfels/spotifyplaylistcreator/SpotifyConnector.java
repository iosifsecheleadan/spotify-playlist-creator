package ch.loewenfels.spotifyplaylistcreator;

import org.apache.hc.core5.net.URIBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Base64;

public class SpotifyConnector {
    public static final String IOSIF_CLIENT_ID = getIosifsClientID();

    public static final String IOSIF_CLIENT_SECRET = getIosifsSecret();

    private static final String SPOTIFY_TOKEN_URI = "https://accounts.spotify.com/api/token";

    private static final String SPOTIFY_PLAYLIST_URI = "https://api.spotify.com/v1/search"; //https://api.spotify.com/v1/artists/{id}
    private final String clientId;
    private final String clientSecret;
    public SpotifyConnector(final String clientId, final String clientSecret) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }
    public String getAccessToken() {
        //curl -X "POST" -H "Authorization: Basic <my_encoded_credentials>" -d grant_type=client_credentials https://accounts.spotify.com/api/token
        HttpRequest request = getAccessTokenRequest();
        HttpResponse<String> responseToken = sendRequest(request);
        String jsonToken = responseToken.body();
        return getAccessTokenFromJsonString(jsonToken);
    }

    public HttpResponse<String> sendRequest(HttpRequest request) {
        HttpResponse<String> response;
        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
        if (response.statusCode() != 200) {
            throw new IllegalStateException("Request Error");
        }
        return response;
    }

    public HttpRequest getAccessTokenRequest() {
        try {
            return HttpRequest.newBuilder()
                    .headers(
                            "Authorization", "Basic " + encodeClientIdAndSecret(),
                            "Content-Type", "application/x-www-form-urlencoded"
                    )
                    .POST(HttpRequest.BodyPublishers.ofString("grant_type=client_credentials"))
                    .uri(new URI(SPOTIFY_TOKEN_URI))
                    .build();
        } catch (URISyntaxException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public String getPlayList(String artist) throws URISyntaxException {
        //  curl -H "Authorization: Bearer <my_access_token>" "https://api.spotify.com/v1/search?q=<some_artist>&type=artist"
        String token = getAccessToken();
        getSearchArtistRequest(token, artist);
        return null;
    }

    private HttpRequest getSearchArtistRequest(String token, String artist) throws URISyntaxException {
        URI uri = new URIBuilder().setPath(SPOTIFY_PLAYLIST_URI)
                .addParameter("q", artist)
                .addParameter("type","artist")
                .build();
        return HttpRequest.newBuilder()
                .headers(
                        "Authorization", "Bearer " + token,
                        "Content-Type", "application/x-www-form-urlencoded"
                )
                .GET()
                .uri(uri)
                .build();
    }

    public String encodeClientIdAndSecret() {
        byte[] clientIdAndSecret = (clientId + ":" + clientSecret).getBytes();
        return new String(Base64.getEncoder().encode(clientIdAndSecret));
    }

    public String getAccessTokenFromJsonString(String jsonToken) {
        JSONObject tokenObject = new JSONObject(jsonToken);
        try {
            return tokenObject.get("access_token").toString();
        } catch (JSONException jse) {
            throw new IllegalStateException("Token not found in answer object");
        }
    }

    private static String getIosifsClientID() {
        return null;
    }

    private static String getIosifsSecret() {
        return null;
    }
}
