package ch.loewenfels.spotifyplaylistcreator;

import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;
import java.net.http.HttpRequest;

import static org.junit.jupiter.api.Assertions.*;

class SpotifyConnectorTest {
    @Test
    public void encodedString() {
        String client = "client";
        String clientSecret = "clientSecret";
        SpotifyConnector connector = new SpotifyConnector(client, clientSecret);
        String result = connector.encodeClientIdAndSecret();
        // expected: encoded "client:clientSecret" by https://www.base64encode.org/
        assertEquals("Y2xpZW50OmNsaWVudFNlY3JldA==", result);
    }

    @Test
    public void getAccessTokenFromJsonString() {
        String client = "client";
        String clientSecret = "clientSecret";
        SpotifyConnector connector = new SpotifyConnector(client, clientSecret);
        String result = connector.getAccessTokenFromJsonString("{\"access_token\": \"test\"}");
        assertEquals("test", result);
    }


    @Test
    void getAccessTokenValid() {
        SpotifyConnector connector = new SpotifyConnector(SpotifyConnector.IOSIF_CLIENT_ID, SpotifyConnector.IOSIF_CLIENT_SECRET);
        final String result = connector.getAccessToken();
        assertNotNull(result);
    }

    @Test
    void getAccessTokenRequest_valid() {
        final SpotifyConnector connector = new SpotifyConnector(SpotifyConnector.IOSIF_CLIENT_ID, SpotifyConnector.IOSIF_CLIENT_ID);
        HttpRequest request = connector.getAccessTokenRequest();
        // todo: test falsch!!
        assertNotNull(request);
    }


    @Test
    void sendRequest_invalid_responseCode400() {
        final SpotifyConnector connector = new SpotifyConnector("wrong Client Id", "wrong Client Secret");
        HttpRequest request = connector.getAccessTokenRequest();
        assertThrows(IllegalStateException.class, () -> connector.sendRequest(request));
    }

    @Test
    void getPlaylist_happyCase() throws URISyntaxException {
        final SpotifyConnector connector = new SpotifyConnector(SpotifyConnector.IOSIF_CLIENT_ID, SpotifyConnector.IOSIF_CLIENT_SECRET);
        String response = connector.getPlayList("Adele"); // 4dpARuHxo51G3z768sgnrY
        assertNotNull(response);
    }
}
